﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float speed;

	public Text countText;

	public Text WinText;

	private Rigidbody rb;

	private int count;

	void Start()
	{
		rb = GetComponent<Rigidbody> ();
		count = 0;
		setCountText ();
		WinText.text = "";
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rb.AddForce (movement * speed);
	}    

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("PickUp")) {
			other.gameObject.SetActive (false);
			count += 1;
			setCountText ();
		}
	}

	void setCountText()
	{
		countText.text = "Count: " + count.ToString (); 
		if (count >= 21) {
			WinText.text = "You Win!";
		}
	}
}  